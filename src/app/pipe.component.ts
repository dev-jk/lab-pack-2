import { Component} from '@angular/core';
       
@Component({
    selector: 'date-comp',
    template: `<div>Без форматирования: {{myDate}}</div>
               <div>С форматированием: {{myDate | date}}</div>
               <div>{{welcome | uppercase}}</div>
               <div>{{welcome | lowercase}}</div>
               <div>{{persentage | percent}}</div>
               <div>{{persentage | currency}}</div>
               <div>{{welcome | slice:3}}</div>
               <div>{{welcome | slice:6:11}}</div>
               <div>{{myDate | date:"dd/MM/yyyy"}}</div>
               <div>{{pi | number:'2.1-2'}}</div>
               <div>{{pi | number:'3.5-5'}}</div>
               <div>{{money | currency:'RUB':'code'}}</div>
               <div>{{money | currency:'RUB':'symbol-narrow'}}</div>
               <div>{{money | currency:'RUB':'symbol':'1.1-1'}}</div>
               <div>{{money | currency:'RUB':'symbol-narrow':'1.1-1'}}</div>
               <div>{{money | currency:'RUB':'тока седня по цене '}}</div>
               <div>{{welcome | slice:6:11 | uppercase}}</div>
               <div>Число до форматирования: {{x}}<br>Число после форматирования: {{x | format}}</div>
               <div>{{users | join}}</div>
               <div>{{users | join:1}}</div>
               <div>{{users | join:1:3}}</div>
               <input [(ngModel)]="x" name="fact">
               <div>Результат: {{x | format}}</div>`
})
export class PipeComponent { 
 
    welcome: string = "Hello World!";
    persentage: number = 0.14; 
    myDate = new Date(1961, 3, 12);
    money: number = 23.45;
    pi: number = 3.1415;
    x: number = 15.45;

    users = ["Tom", "Alice", "Sam", "Kate", "Bob"];
    
}