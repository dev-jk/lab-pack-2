import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {User} from './user';
import {map, catchError} from 'rxjs/operators';
  
@Injectable()
export class HttpService{
  
    constructor(private http: HttpClient){ }
      
    getData(){
        return this.http.get('assets/users.json')
    }

    getUsers() : Observable<User[]> {
        return this.http.get('users.json').pipe(map(data=>{
            let userList = data["userList"];
            return userList.map(function(user:any) {
                return {name: user.userName, age: user.userAge};
              });
        }),
        catchError(err => {  
            console.log(err); 
            return throwError(err);
        }))
    };

    getSum(num1: number, num2: number){
        return this.http.get('http://localhost:3000/sum?num1=' + num1 + "&num2=" + num2);
    }

    getSum2(num1: number, num2: number){
        const params = new HttpParams()
        .set('num1', num1.toString())
        .set('num2', num2.toString());
        return this.http.get('http://localhost:3000/sum', {params});
    }

    postData(user: User){
          
        const body = {name: user.name, age: user.age};
        return this.http.post('http://localhost:3000/postuser', body); 
    }

    postData2(user: User){
         
        const myHeaders = new HttpHeaders().set('Authorization', 'my-auth-token');
          
        return this.http.post('http://localhost:3000/postuser', user, {headers:myHeaders}); 
    }


}