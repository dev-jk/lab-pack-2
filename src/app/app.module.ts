import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }   from './app.component';
import { HttpClientModule }   from '@angular/common/http';
import { CalcComponent } from './calc.component';
import { PostComponent } from './post.component';

import {Routes, RouterModule} from '@angular/router';

import { AboutComponent }   from './about.component';
import { HomeComponent }   from './home.component';
import { NotFoundComponent }   from './not-found.component';

import { ItemComponent } from './item.component';
import { ItemStatComponent }   from './item.stat.component';
import { ItemDetailsComponent }   from './item.details.component';

import { AboutGuard }   from './about.guard';
import {ExitAboutGuard} from './exit.about.guard';

import { PipeComponent } from './pipe.component';

import { FormatPipe} from './format.pipe';
import { JoinPipe} from './join.pipe';


const itemRoutes: Routes = [
    { path: 'details', component: ItemDetailsComponent},
    { path: 'stat', component: ItemStatComponent},
];

const appRoutes: Routes =[
    { path: '', component: HomeComponent},
    { path: 'about', component: AboutComponent, canActivate: [AboutGuard], canDeactivate: [ExitAboutGuard]},
    { path: 'pipe', component: PipeComponent},
    { path: 'item/:id', component: ItemComponent},
    { path: 'item/:id', component: ItemComponent, children: itemRoutes},
    { path: 'contact', redirectTo: '/about', pathMatch:'full'},  
    { path: '**', redirectTo: '/' }
];


@NgModule({
    imports:      [ BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes) ],
    declarations: [ AppComponent, CalcComponent, PostComponent, HomeComponent, AboutComponent, PipeComponent, NotFoundComponent,
                    ItemComponent, ItemDetailsComponent, ItemStatComponent, FormatPipe, JoinPipe],
    providers:    [ AboutGuard, ExitAboutGuard ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }