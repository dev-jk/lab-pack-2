import { Component, OnInit} from '@angular/core';
import { HttpService} from './http.service';
import {User} from './user';
import {Router} from '@angular/router';

export class Item{
    id: number;
    product: string;
    price: number;
}
   
@Component({
    selector: 'my-app',
    template: `<ul>
                <li *ngFor="let user of users">
                <p>Имя пользователя: {{user?.name}}</p>
                <p>Возраст пользователя: {{user?.age}}</p>
                </li>
            </ul>
            <calc-comp></calc-comp>
            <post-comp></post-comp>
            <div>
                    <ul class="nav">
                        <li routerLinkActive="active" [routerLinkActiveOptions]="{exact:true}">
                            <a routerLink="">Главная</a>
                        </li>
                        <li routerLinkActive="active">
                            <a routerLink="/about">О сайте</a>
                        </li>
                        <li routerLinkActive="active">
                            <a routerLink="/pipe">Pipe Test</a>
                        </li>
                        <li routerLinkActive="active">
                            <a [routerLink]="['item', '5']"
                            [queryParams]="{'product':'phone', 'price': 200}">Item 5</a>
                        </li>
                        <li routerLinkActive="active">
                            <a [routerLink]="['item', '8']"
                            [queryParams]="{'product':'tablet'}">item 8</a>
                        </li>
                        <li routerLinkActive="active">
                            <a routerLink="">Главная</a> |
                            <a routerLink="/item/5/details">Информация о товаре</a> |
                            <a routerLink="/item/5/stat">Статистика товара</a>
                        </li>
                        
                    </ul>
                    <router-outlet></router-outlet>
                    <button (click)="goHome()">На главную</button>
                    <button (click)="goToAbout()">About</button>
                    <div  class="form-group">
                        <h3>Параметры объекта</h3>
                        <input type="number" [(ngModel)]="item.id" class="form-control" placeholder="Номер модели"/><br />
                        <input type="number" [(ngModel)]="item.price" class="form-control" placeholder="Цена" /><br />
                        <input [(ngModel)]="item.product" class="form-control" placeholder="Товар" /><br />
                        <button (click)="goToItem(item)" class="btn">Перейти</button>
                    </div>
               </div>`,
    providers: [HttpService]
})
export class AppComponent implements OnInit { 
   
    users: User[]=[];
    error:any;
    item: Item=new Item();
      
    constructor(private httpService: HttpService, private router: Router){}
      
    ngOnInit(){
          
        this.httpService.getUsers().subscribe(
            data => this.users=data,
            error => {this.error = error.message; console.log(error);}
        );
    }

    goHome(){
         
        this.router.navigate(['']);
    }

    goToAbout(){

        this.router.navigate(['/about']);
    }

    goToItem(myItem: Item){
             
        this.router.navigate(
            ['/item', myItem.id], 
            {
                queryParams:{
                    'product': myItem.product, 
                    'price': myItem.price
                }
            }
        );
    }
}