import { Component} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
  
@Component({
    selector: 'item-info',
    template: `<h3>Модель {{id}}</h3>
                <router-outlet></router-outlet>
                <div>Товар: {{product}}</div>
                <div>Цена: {{price}}</div>`
                
})
export class ItemComponent { 
     
    // id: number;
    // private subscription: Subscription;
    // constructor(private activateRoute: ActivatedRoute){
         
    //     this.subscription = activateRoute.params.subscribe(params=>this.id=params['id']);
    // }

    id: number;
    product: string;
    price: string;

    private routeSubscription: Subscription;
    private querySubscription: Subscription;
     
    constructor(private route: ActivatedRoute){}
    ngOnInit() {
        this.route.paramMap.pipe(
            switchMap(params => params.getAll('id'))
        )
        .subscribe(data=> this.id = +data);

        this.querySubscription = this.route.queryParams.subscribe(
            (queryParam: any) => {
                this.product = queryParam['product'];
                this.price = queryParam['price'];
            }
        );

      }
}